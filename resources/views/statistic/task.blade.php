@extends('...master')

@section('header')
@endsection

@section('content')
    <div class="col-sm-9">
        <div class="row">
            <table class="table">
                <tr>
                    <th>
                        Задача
                    </th>
                    <th>
                        Пользователь
                    </th>
                    <th>
                        Дата
                    </th>
                </tr>
                @foreach($statistics as $item)
                    <tr>
                        <th>
                            <a href="{{ '/task/'.$item->task_id.'/edit' }}">{{ $item->task_name }}</a>
                        </th>
                        <th>
                            {{ $item->user_name }}
                        </th>
                        <th>
                            {{ $item->datetime }}
                        </th>
                    </tr>
                @endforeach
            </table>
        </div>
    </div>
@endsection
