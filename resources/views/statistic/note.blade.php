@extends('...master')

@section('header')
@endsection

@section('content')
    <div class="col-sm-9">
        <div class="row">
            <table class="table">
                <tr>
                    <th>
                        Заметка
                    </th>
                    <th>
                        Пользователь
                    </th>
                    <th>
                        Дата
                    </th>
                </tr>
                @foreach($statistics as $item)
                    <tr>
                        <th>
                            @if(isset($item))
                                <a href="{{ '/note/'.$item->note_id }}">{{ $item->note_name }}</a>
                            @else
                                Аноним
                            @endif
                        </th>
                        <th>
                            {{ $item->user_name }}
                        </th>
                        <th>
                            {{ $item->datetime }}
                        </th>
                    </tr>
                @endforeach
            </table>
        </div>
    </div>
@endsection
