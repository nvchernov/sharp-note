<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/html">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="{{ asset('css/bootstrap.min.css')}}">
	<link rel="stylesheet" href="{{ asset('css/style.css')}}">
	<link rel="stylesheet" href="{{ asset('css/bootstrap-tagsinput.css')}}">
	<link rel="stylesheet" href="{{ asset('css/bootstrap-dialog.css')}}">
	<script src="{{ asset('js/jquery.min.js')}}"></script>
	<script src="{{ asset('js/bootstrap.min.js')}}"></script>
	<script src="{{ asset('js/bootstrap-tagsinput.js')}}"></script>
	<script src="{{ asset('js/bootstrap-dialog.js')}}"></script>
	<title>paper</title>
	<script>
    var csrft = {!! json_encode(array('_token' => csrf_token())) !!}
	 </script>
    @yield('header')
</head>
<body>
<nav class="navbar navbar-default navbar-fixed-top">
    <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#c-nav-bar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <div class="navbar-header">
            <a class="navbar-brand brand-text" href="/home">#note</a>
            <a href="#" id="note-list-toggle" class="brand-button navbar-brand"><span id="button-toggle-icon" class="glyphicon glyphicon-list-alt"></span></a>
        </div>
    </div>
    <div class="collapse navbar-collapse" id="c-nav-bar">
        <ul class="nav navbar-nav">
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                Заметки
                <span class="caret"></span></a>
                <ul class="dropdown-menu" role="menu">
                    <li><a href="/note/">К заметкам</a></li>
                    <li class="divider"></li>
                    <li><a href="/note/trash">Корзина</a></li>
                    <li><a href="/statistic/note/">Статистика просмотров</a></li>
                </ul>
            </li>
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                Задачи
                <span class="caret"></span></a>
                <ul class="dropdown-menu" role="menu">
                    <li><a href="/task">К задачам</a></li>
                    <li class="divider"></li>
                    <li><a href="/statistic/task">Статистика просмотров</a></li>
                </ul>
            </li>
        </ul>
        <ul class="nav navbar-nav navbar-right">
        @if(Auth::user() != null)
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                <span class="glyphicon glyphicon-user"></span>&nbsp;{{ Auth::user()->name }}
                <span class="caret"></span></a>
                <ul class="dropdown-menu" role="menu">
                    <li><a href="#">Профиль</a></li>
                    <li class="divider"></li>
                    <li><a href="/auth/logout">Выйти</a></li>
                </ul>
            </li>
        @else
        <li>
            <a href="/auth/register">Регистрация</a>
        </li>
        <li>
            <a href="/auth/login">Вход</a>
        </li>
        @endif
        </ul>
    </div>
</nav>
<div class="container-fluid" style="height: 100%;">
<div class="row" style="height: 100%;">
@yield('left_block')
@yield('content')
</div>
</div>
<footer class="footer">
    <div class="container">

    </div>
</footer>
<script>
var Control = function(param) {
    if(!!param.id) {
        this.id = param.id;
        this.dom = $('#'+param.id);
    } else if(!!param.class) {
        this.class = param.class;
        this.dom = $('.'+param.id);
    }
    if(!!param.click)
        this.dom.click(param.click);
    this.val = function() {
        return this.dom.val();
    };
    this.d = this.dom;
};
var NavBarForm = function() {
    this.edit_form_visible = true;
    this.toggleList = Control({id:"note-list-toggle",click:function(){$('.note-list').toggleClass('disabled');}});
};
var navBarForm = new NavBarForm();
</script>
<script src="{{ asset('js/note.js')}}"></script>
<script>
var noteForm = new NoteForm({{ isset($note) ? $note->id : 'undefined' }});
</script>
</body>
</html>