@extends('master')
@section('header')
<script>
</script>
<script src="{{ asset('js/note.js') }}"></script>
@endsection
@section('left_block')
    <div class="col-sm-3 note-list">
        <div id="CNoteAdd" class="row note-add">
            <a href="/note/create">
                <span class="glyphicon glyphicon-plus"></span> Новая заметка
            </a>
        </div>
        @include('note.list')
    </div>
@endsection
@section('content')
    <div class="col-sm-9">
        <form method="post" action="/note/put_file" enctype="multipart/form-data">
            <label>Название заметки</label>
            <input name="name" id="textname" type="text" class="form-control" placeholder="Название" value="">

            <label>Текст заметки</label>
            <textarea name="text" id="textedit" class="form-control" rows="12" placeholder="Текст"></textarea><br/>
            <label>Добавьте файлы</label>
            <input name="files[]" type="file"  multiple><br/>
            <input type="submit" value="Создать" class="btn btn-default btn-sm" >
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
        </form>
    </div>
@endsection
