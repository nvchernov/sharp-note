@extends('master')
@section('left_block')
    <div class="col-sm-3 note-list">
        <div id="CNoteAdd" class="row note-add">
            <a href="/note/create">
                <span class="glyphicon glyphicon-plus"></span> Новая заметка
            </a>
        </div>
        @include('note.list')
    </div>
@endsection
@section('content')
<div class="col-sm-9">
    <div class="row">
        <div class="col-xs-12">
            <div class="row content-item note-menu-top">
                <div class="btn-group btn-group-md" role="group">
                    <a id="CNoteSave" class="btn btn-default">
                        <span class="glyphicon glyphicon-floppy-disk"></span>
                    </a>
                    <a class="btn btn-default" href="/note/{{$note->id}}/remove">
                        <span class="glyphicon glyphicon-trash"></span>
                    </a>
            </div>
                <div class="pull-right">
                    <div class="btn-group btn-group-md" role="group">
                        <a class="btn btn-default" href="/note/{{$note->id}}/stat">
                            <span class="glyphicon glyphicon-stats"></span>
                        </a>
                        <button id="CEditButton" class="btn btn-default"><span class="glyphicon glyphicon-pencil"></span></button>
                        <button id="CLinkButton" class="btn btn-default"><span class="glyphicon glyphicon-link"></span></button>
                        <button id="CFileButton" class="btn btn-default"><span class="glyphicon glyphicon-file"></span></button>
                    </div>
                    <span class="note-menu-top-time">
                        Создана: {{ $note->created_at }} Обновлена: {{ $note->updated_at }}
                    </span>
                </div>
            </div>
            <div class="content-separator"></div>
            <div id="CEditForm" class="row content-item edit-form"> <!-- Форма редактирования заметки -->
                <input id="CNoteName" class="c-note-name" type="text" placeholder="Название" value="{{ $note->name }}"><br/>
                <textarea id="CNoteText" class="c-note-text" placeholder="Заметка">{{ $note->text }}</textarea>
            </div>
            <div id="CFileForm" class="row content-item collapse">  <!-- Форма работы с файлами -->
            @if(isset($file_list) && count($file_list) > 0)
                <ul>
                @foreach($file_list as $i)
                    <li>
                        <a href="/download/{{$i->id}}">{{$i->name}}</a>
                        <span class="glyphicon glyphicon-remove"></span>
                    </li>
                @endforeach
                </ul>
            @else
                <p>Тут нет файлов : )</p>
            @endif
            <!-- TODO форма работы с файлами -->
            </div>
            <div id="CLinkForm" class="row col-xs-12 col-lg-6 content-item link-form collapse">  <!-- Форма ссылкой на заметку -->
                <label>Ссылка на вашу заметку:</label>
                <div class="form-inline">
                    <input type="text" id="CLink" onclick="this.select();"
                           class="form-control" placeholder="Тут будет ссылка на заметку"
                           value="{{isset($note) && isset($note->hash_link) ? 'http://'.$_SERVER['HTTP_HOST'].'/p/'.$note->hash_link : "" }}" readonly/>
                </div>
                <label>Отправьте ссылку по e-mail вашим друзьям:</label>
                <div class="form-inline">
                <input type="text" id="CMailList" data-role="tagsinput" class="form-control" value="" />
                    <button id="CGetLinkBtn" class="btn btn-default">
                        <span class="glyphicon {{!isset($note->hash_link) ? "glyphicon-refresh" : "glyphicon-send"}}"></span>
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection