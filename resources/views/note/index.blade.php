@extends('master')
@section('header')
<script>
var _paper_id = {{ isset($note->id) ? $note->id : "undefined" }};
</script>
<script src="{{ asset('js/note.js') }}"></script>
@endsection
@section('left_block')
    <div class="col-sm-3 note-list">
        <div id="CNoteAdd" class="row note-add">
            <a href="/note/create">
                <span class="glyphicon glyphicon-plus"></span> Новая заметка
            </a>
        </div>
        @include('note.list')
    </div>
@endsection
@section('content')

    <p>Выберите или добавьте заметку</p>

@endsection
