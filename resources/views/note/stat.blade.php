@extends('master')
@section('header')
@endsection
@section('left_block')
    <div class="col-sm-3 note-list">
        <div id="CNoteAdd" class="row note-add">
            <a href="/note/create">
                <span class="glyphicon glyphicon-plus"></span> Новая заметка
            </a>
        </div>
        @include('note.list')
    </div>
@endsection
@section('content')
<div class="col-sm-9">
    <div class="row">
<table class="table">
    <tr>
        <td>
            Название заметки
        </td>
        <td>
            Дата изменения
        </td>
        <td>
            Откатить
        </td>
    </tr>
    @foreach($stored_notes as $item)
    <tr>
        <td>
            {{$item->name}}
        </td>
        <td>
            {{$item->updated_at}}
        </td>
        <td>
            <a href="/note/{{$item->id}}/rollback"><span class="glyphicon glyphicon-download-alt"</span></a>
        </td>
    </tr>
    @endforeach
</table>
</div>
</div>
@endsection
