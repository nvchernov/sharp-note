{{-- Шаблон списока заметок --}}
@if(isset($note_list))
@foreach($note_list as $i)
<div class="row note" note-id="{{$i->id}}">
    <div class="col-xs-1">
        <input type="checkbox">
    </div>
    <div class="col-xs-10 note-name">
        <a href="/note/{{ $i->id }}/edit">{{ $i->name }}</a>
    </div>
    <div class="col-xs-1">
        {{--<span class="glyphicon glyphicon-link c-popover-btn" data-toggle="popover" note-id="{{$i->id}}"></span>--}}
        <span class="glyphicon glyphicon-remove pull-right" onclick="noteForm.removeNote({{ $i->id }})"></span>
    </div>
</div>
@endforeach
@endif