@extends('...master')
@section('header')
<script src="{{ asset('js/note.js') }}"></script>
@endsection
@section('left_block')
    <div class="col-sm-3 note-list">
        <div id="CNoteAdd" class="row note-add">
            <a href="/note/create">
                <span class="glyphicon glyphicon-plus"></span> Новая заметка
            </a>
        </div>
        @include('note.list')
    </div>
@endsection
@section('content')
<div class="col-sm-9">
    <div class="row">
    @if(isset($deleted_notes) && count($deleted_notes) > 0)
        <table class="table">
            <tr>
                <td>
                    Заметка
                </td>
                <td>
                    Время удаления
                </td>
                <td>
                    Восстановить
                </td>
            </tr>
            @foreach($deleted_notes as $item)
            <tr>
                <td>
                    {{ $item->name }}
                </td>
                <td>
                    {{ $item->deleted_at }}
                </td>
                <td>
                    <a href="/note/{{ $item->id }}/restore"><span class="glyphicon glyphicon-export"></span></a>
                </td>
            </tr>
            @endforeach
        </table>
    @else
    Упс, похоже что ваша корзина пустая ...
    @endif
    </div>
</div>
@endsection
