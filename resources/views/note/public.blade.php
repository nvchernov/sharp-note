@extends('master')
@section('header')
<script>
var _paper_id = {{ isset($note->id) ? $note->id : "undefined" }};
</script>
<script src="{{ asset('js/note.js') }}"></script>
@endsection
@section('content')
@if(isset($note->text))
<div class="panel panel-default">
    <div class="panel-heading">
        <h1 class="panel-title">
            {{$note->name}}<br/>
            <small>Создана: {{$note->created_at}} Обновлена: {{$note->updated_at}}</small>
        </h1>
    </div>
    <div class="panel-body">
        {{$note->text}}
    </div>
</div>
@endif
@endsection
