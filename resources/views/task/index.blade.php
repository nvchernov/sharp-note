@extends('master')
@section('header')
<link rel='stylesheet' href="{{ asset('css/bootstrap-datetimepicker.css')  }}" />
<link rel='stylesheet' href="{{ asset('css/fullcalendar.css')  }}" />
<link rel='stylesheet' href="{{ asset('css/style.css')  }}" />
<script src="{{ asset('js/moment.js')   }}"></script>
<script src="{{ asset('js/fullcalendar.js') }}"></script>
<script src="{{ asset('js/ru-moment.js') }}"></script>
<script src="{{ asset('js/bootstrap-datetimepicker.js') }}"></script>
<script src="{{ asset('js/ru.js') }}"></script>
<script>
var tasks = {!! json_encode($tasks, JSON_UNESCAPED_UNICODE)!!}
//при нажатии на окно убирает все поповеры(менюшки)
$(window).on('click', function (e) {
    $('[data-toggle="popover"]').each(function () {
        if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
            $(this).popover('hide');
        }
    });
});
</script>
@endsection
@section('content')
<div class="calendar-form">
    <div class="col-xs-12 content-item note-menu-top">
        <a href="/task/create">
            <span class="glyphicon glyphicon-plus"></span>
        </a>
    </div>
    <div class="col-xs-12 content-separator"></div>
    <div id="calendar" class="col-xs-12 content-item"></div>
</div>

<script>
var Rep = function(){
    this.update = function(params){
        $.ajax({
            type: "PUT",
            url: "/task/"+params.id,
            data: "_token=" + csrft._token + "&json=" + JSON.stringify(params),
            success: function(msg)
            {
                if('string' == typeof msg){
                   updateTasks(JSON.parse(msg));
                }
            },
            error: function(msg) {
                alert('error' + msg);
            }
        });
    };

    this.destroy = function(params){
        $.ajax({
            type: "DELETE",
            url: "/task/"+params.id,
            data: "_token=" + csrft._token + "&json=" + JSON.stringify(params),
            success: function(msg)
            {
                if('string' == typeof msg){
                    deleteTasks(JSON.parse(msg).deleted);
                }
            },
            error: function(msg) {
              alert('error' + msg);
            }
        });
    };
};
var rep = new Rep();

var calendar_cfg = {
    buttons: { prevMonth:true, nextMonth: true },
    droppable: true,
    editable: true,
    eventDrop: function(event, delta, revertFunc) {
        params = {
            new_date: event.start.format('DD.MM.YYYY H:mm'),
            id:event._id
        };
        showDiagUpd(params);
        revertFunc();
        //window.rep.update(params);
    },
    eventAfterAllRender : function(){
        $.each($('.fc-day[data-date]'), function(index, val){
            var elem = $(val);
            var count = elem.attr('event_count');
            var date = elem.attr('data-date');

            if(count == 0)
                elem.css('background-color', '#fff');
            else if(count == 1)
                elem.css('background-color', '#fee');
            else if(count == 2 || count == 3)
                elem.css('background-color', '#fdd');
            else if(count == 4 || count == 5)
                elem.css('background-color', '#fcc');
            else if(count == 6 || count == 7)
                elem.css('background-color', '#fbb');
            else if(count == 8 || count == 9)
                elem.css('background-color', '#faa');
            else if(count >= 10)
                elem.css('background-color', '#f99');
        });
    },
    eventBeforeAllRender : function(){
        $('.fc-day[data-date]').css('background-color', '#fff').attr('event_count', '0');
    },
    eventRender: function (event, element, view) {
        var content = '';
        content += '<a href = "/task/'+event.id+'/edit" class="btn-sm"><span class="glyphicon glyphicon-pencil"></span></a>';
        //content += '<a onclick="taskForm.show(' + event.id + ')" class="btn-sm"><span class="glyphicon glyphicon-search"></span></a>';
        content += '<a onclick="showDiagRem(' + event.id + ')" class="btn-sm"><span class="glyphicon glyphicon-remove"></span></a>';
        element.attr('data-toggle', 'popover');
        element.attr('data-html', 'true');
        element.popover({
            title: event.name,
            placement: 'top',
            content: content
        });
        //отрисовка цвета
        var dateString = event.start.format('YYYY-MM-DD');
        var cell = $('.fc-day[data-date="' + dateString + '"]');
        var count = 1 * cell.attr('event_count');
        cell.attr('event_count' ,  count + 1);
    },
    'firstDay' : '1',
    'editable' : true,
    eventLimit : 3,
    header: {
        left: 'prev,next today',
        center: 'title'
    },
    events: tasks

};
var calendar = $('#calendar');
calendar.fullCalendar(calendar_cfg);

var showDiagRem = function(task_id) {
    BootstrapDialog.show({
        title: 'Удаление задачи',
        message: 'Удалить ... задачи',
        buttons:
        [
            {label: 'Все', action: function(dialogItself){
                window.rep.destroy({id:task_id, all:true});
                dialogItself.close();
            }},
            {label: 'Это', action: function(dialogItself){
                window.rep.destroy({id:task_id, self:true});
                dialogItself.close();
            }},
            {label: 'Это и предыдущ.', action: function(dialogItself){
                window.rep.destroy({id:task_id, self:true, previous:true});
                dialogItself.close();
            }},
            {label: 'Это и следующ..', action: function(dialogItself){
                window.rep.destroy({id:task_id, self:true, next:true});
                dialogItself.close();
            }},
            {label: 'Все кроме этого', action: function(dialogItself){
                window.rep.destroy({id:task_id, all:true, not_self:true});
                dialogItself.close();
            }},
            {label: 'Отмена',action: function(dialogItself){
                dialogItself.close(dialogItself);
            }}
        ]
    });
};
var showDiagUpd = function(params) {
    BootstrapDialog.show({
        title: 'Перетащить задачу',
        message: 'Перетащить задачи ...',
        buttons:
        [
            {label: 'Все', action: function(dialogItself){
                window.rep.update({id:params.id, all:true, new_date:params.new_date});
                dialogItself.close();
            }},
            {label: 'Это', action: function(dialogItself){
                window.rep.update({id:params.id, self:true, new_date:params.new_date});
                dialogItself.close();
            }},
            {label: 'Это и предыдущ.', action: function(dialogItself){
                window.rep.update({id:params.id, self:true, previous:true, new_date:params.new_date});
                dialogItself.close();
            }},
            {label: 'Это и следующ..', action: function(dialogItself){
                window.rep.update({id:params.id, self:true, next:true, new_date:params.new_date});
                dialogItself.close();
            }},
            {label: 'Все кроме этого', action: function(dialogItself){
                window.rep.update({id:params.id, all:true, not_self:true, new_date:params.new_date});
                dialogItself.close();
            }},
            {label: 'Отмена',action: function(dialogItself){
                dialogItself.close(dialogItself);
            }}
        ]
    });
};
var deleteTasks = function(ids)
{
    ids.forEach(function(id){
        calendar.fullCalendar('removeEvents', id);
    });
};

var updateTasks = function(events) {
    $.each(events, function(i,v){
        var event = calendar.fullCalendar('clientEvents', v.id);
        event[0].start = v.start_date;
        event[0].end = v.end_date;
        calendar.fullCalendar('updateEvent', event[0]);
    });
};


</script>

@endsection
