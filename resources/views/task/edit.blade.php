@extends('master')
@section('header')
<link rel='stylesheet' href="{{ asset('css/bootstrap-datetimepicker.css')  }}" />
<link rel='stylesheet' href="{{ asset('css/fullcalendar.css')  }}" />
<link rel='stylesheet' href="{{ asset('css/style.css')  }}" />
<script src="{{ asset('js/moment.js')   }}"></script>
<script src="{{ asset('js/fullcalendar.js') }}"></script>
<script src="{{ asset('js/ru-moment.js') }}"></script>
<script src="{{ asset('js/bootstrap-datetimepicker.js') }}"></script>
<script src="{{ asset('js/moment-range.js') }}"></script>
@endsection
@section('content')
    <div class="bs-callout bs-callout-sharp-note">
        <h4>Изменить задачу</h4>
        <br/>
        <form method="POST" action="/task/{{$task->id}}">
            <label>Название задачи</label><br/>
            <input name="name" type="text" class="form-control" placeholder="" value="{{$task->name or ""}}"><br/>

            <label>Текст задачи</label><br/>
            <textarea name="text" class="form-control" placeholder="" rows="12">{{$task->text or ""}}</textarea><br/>

            <label>Дата начала</label><br/>
            <div class='input-group date' id='textbox-date-start-dp'>
                <input id="start_date" name="start_date" type="datetime" class="form-control" placeholder="" value="{{$task->start_date or ""}}"><br/>
            </div>

            <label>Дата конца</label><br/>
            <div class='input-group date' id='textbox-date-end-dp'>
                <input id="end_date" name="end_date" type="datetime" class="form-control" placeholder="" value="{{$task->end_date  or ""}}"><br/>
            </div>
            {!! isset($task) ? "<input type='hidden' name='_method' value='PUT'>" : "" !!}
            <input type="hidden" name="_token" value="{{ csrf_token() }}">

            <input type="submit" value="Сохранить">
        </form>
    </div>
    <div class="bs-callout bs-callout-sharp-note">  <!-- Форма ссылкой на заметку -->
        <h4>Отправить заметку</h4>
        <br/>
        <label>Ссылка на вашу заметку:</label>
        <div class="form-inline">
            <input type="text" id="CLink" onclick="this.select();"
               class="form-control" placeholder="Тут будет ссылка на заметку"
               value="{{isset($task) && isset($task->hash_link) ? 'http://'.$_SERVER['HTTP_HOST'].'/t/'.$task->hash_link : "" }}" readonly/>
        </div>
        <label>Отправьте ссылку по e-mail вашим друзьям:</label>
        <div class="form-inline">
        <input type="text" id="CMailList" data-role="tagsinput" class="form-control" value="" />
            <button id="CGetLinkBtn" class="btn btn-default">
                <span class="glyphicon {{!isset($task->hash_link) ? "glyphicon-refresh" : "glyphicon-send"}}"></span>
            </button>
        </div>
    </div>

<script>
    var task_id = {{ $task->id }};
    $(function () {
        $('#start_date').datetimepicker();
    });
    $(function () {
        $('#end_date').datetimepicker();
    });

    var getLinkAjax = function(){

        var json = {
            'emails': $('#CMailList').tagsinput('items').length > 0 ? $('#CMailList').tagsinput('items') : null,
            'need_link': true
        };
        $.ajax({
            type: 'POST',
            url: '/task/link_item/' + window.task_id,
            data: '_token=' + csrft._token + '&json=' + JSON.stringify(json),
            success: function(msg1){
                var resp = JSON.parse(msg1);

                $("#CLink").val(resp.link);
            }
        });
    };
    $('#CGetLinkBtn').click(getLinkAjax);
</script>
@endsection
