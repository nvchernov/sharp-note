@extends('master')
@section('header')
<link rel='stylesheet' href="{{ asset('css/bootstrap-datetimepicker.css')  }}" />
<link rel='stylesheet' href="{{ asset('css/fullcalendar.css')  }}" />
<link rel='stylesheet' href="{{ asset('css/style.css')  }}" />
<script src="{{ asset('js/moment.js')   }}"></script>
<script src="{{ asset('js/fullcalendar.js') }}"></script>
<script src="{{ asset('js/ru-moment.js') }}"></script>
<script src="{{ asset('js/bootstrap-datetimepicker.js') }}"></script>
<script src="{{ asset('js/moment-range.js') }}"></script>
@endsection
@section('content')
<form method="POST" action="/task">
    <div class="edit-panel col-sm-12">
        <label>Название задачи</label><br/>
        <input name="name" type="text" class="form-control" placeholder="" value=""><br/>

        <label>Текст задачи</label><br/>
        <textarea name="text" class="form-control" placeholder="" rows="12"></textarea><br/>

        <label>Кратность шага</label>
        <input name="multiplicity" type="text" class="form-control" placeholder="" value=""><br/>

        <label>Тип шага</label><br/>
        <select name="step_type">
            <option value="0">Час</option>
            <option value="1">День</option>
            <option value="2">Неделя</option>
            <option value="3">Месяц</option>
            <option value="4">Год</option>
        </select><br/>

        <label>Количество шагов</label><br/>
        <input name="step_amount" type="text" class="form-control" placeholder="" value=""><br/>

        <label>Дата начала</label><br/>
        <div class='input-group date' id='textbox-date-start-dp'>
            <input id="start_date" name="start_date" type="datetime" class="form-control" placeholder="" value=""><br/>
        </div>

        <label>Дата конца</label><br/>
        <div class='input-group date' id='textbox-date-end-dp'>
            <input id="end_date" name="end_date" type="datetime" class="form-control" placeholder="" value=""><br/>
        </div>

        <input type="hidden" name="_token" value="{{ csrf_token() }}">

        <input type="submit" value="Создать">

    </div>
    <script>
        $(function () {
            $('#start_date').datetimepicker();
        });
        $(function () {
            $('#end_date').datetimepicker();
        });
    </script>
</form>
@endsection
