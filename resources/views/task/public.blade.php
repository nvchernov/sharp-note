@extends('master')
@section('header')
<script>
var _paper_id = {{ isset($rep_task->id) ? $rep_task->id : "undefined" }};
</script>
<script src="{{ asset('js/note.js') }}"></script>
@endsection
@section('content')
@if(isset($rep_task->text))
<div class="panel panel-default">
    <div class="panel-heading">
        <h1 class="panel-title">
            {{$rep_task->name}}<br/>
            <small>Создана: {{$rep_task->created_at}} Обновлена: {{$rep_task->updated_at}}</small>
        </h1>
    </div>
    <div class="panel-body">
        {{$rep_task->text}}
    </div>
</div>
@endif
@endsection
