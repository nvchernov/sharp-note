<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Note;
use App\NoteStatistics;
use App\RepTask;
use App\TaskStatistics;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PublicController extends Controller {
    /**
     * Отображает заметку по ссылке
     *
     * @param Request $request
     * @return \Illuminate\View\View
     */
    public function getNote(Request $request)
    {
        $hash = $request->segment(2);
        $user = Auth::user();
        $note = Note::get_paper_by_link($hash);

        if(isset($note))
            \App\NoteStatistics::addStatistic($user, $note);

        return view('note.public',
            ['note' => isset($note) ? $note : null]
        );
    }

    /**
     * Отображает задачу по ссылке
     *
     * @param Request $request
     * @return \Illuminate\View\View
     */

    public function getTask(Request $request)
    {
        $hash = $request->segment(2);
        $user = Auth::user();
        $task = \App\RepTask::get_task_by_link($hash);

        $rep_task = RepTask::get_task_by_link($hash);

        if(isset($task))
            TaskStatistics::addStatistic($user, $rep_task);

        return view('task.public',
            ['rep_task' => isset($rep_task) ? $rep_task : null]
        );
    }
}
