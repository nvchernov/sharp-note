<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\NoteStatistics;
use App\TaskStatistics;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class StatisticController extends Controller {

    /**
     * Отображает статистику просмотров заметок
     *
     * @return \Illuminate\View\View
     */
    public function noteStatistic()
    {
        $statistic = NoteStatistics::getStatisticByUser(Auth::user()->id);
        return view('statistic.note',
            [
                'statistics' => $statistic
            ]);
    }

    /**
     * Отображает статистику просмотров задач
     *
     * @return \Illuminate\View\View
     */
    public function taskStatistic()
    {
        $statistic = TaskStatistics::getStatisticByUser(Auth::user()->id);
        return view('statistic.task',
            [
                'statistics' => $statistic
            ]);
    }
}
