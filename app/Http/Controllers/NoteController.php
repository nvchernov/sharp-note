<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Note;
use App\StoredNote;
use App\NoteFileModel;
use App\Providers\RepositoryServiceProvider;
use GuzzleHttp\Subscriber\Redirect;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Contracts\Container;

class NoteController extends Controller {

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $noteList = Note::getNoteList(Auth::user()->id);
        return view('note.index',
            [
                'note_list' => $noteList
            ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $noteList = Note::getNoteList(Auth::user()->id);
        return view('note.create',
            [
                'note_list' => $noteList
            ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $note = new \App\Note();
        $user_id = Auth::user()->id;

        //создать заметку
        $json = json_decode(\Request::input('json'));
        $note->name = \Request::input('name');
        $note->text = \Request::input('text');
        $note->users_id = $user_id;
        $note->save();

        //получить файлы
        if(\Request::hasFile('files'))
        {
            $files = \Request::file('files');

            foreach($files as $file)
            {
                if($file->isValid())
                {
                    $note->addFile($file);
                }
                else
                {
                    //TODO добавить и организовать вывод ошибок
                }
            }
        }
        return \Redirect::to('/note/' . $note->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $user_id = Auth::user()->id;
        $not_found = false;
        $removed = false;
        $noteList = Note::getNoteList(Auth::user()->id);
        $note = Note::withTrashed()
            ->where('users_id', '=', $user_id)
            ->find($id);

        if(!isset($note))
            $not_found = true;
        else
            $removed = $note->trashed();
        return view('note.show',
            [
                'note_list' => $noteList,
                'note' => $note,
                'removed' => $removed,
                'not_found' => $not_found
            ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $user_id = Auth::user()->id;
        $noteList = Note::getNoteList(Auth::user()->id);
        $note = Note::where('users_id', '=', $user_id)->find($id);
        $files = \App\File::getFileList(Auth::user()->id, $id);
        return view('note.edit',
            [
                'note_list' => $noteList,
                'note' => $note,
                'file_list' => $files
            ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        $user_id = Auth::user()->id;
        $input_arr = json_decode(\Request::input('json'), true);

        $note = Note::where('users_id', '=', $user_id)->find($id);

        if($note->name == $input_arr['name'] && $note->text == $input_arr['text'])
            return;

        $note->name = $input_arr['name'];
        $note->text = $input_arr['text'];
        $note->save();

        //создание записи в хранилище изменений
        $stored_note = new \App\StoredNote();
        $stored_note->name = $input_arr['name'];
        $stored_note->text = $input_arr['text'];
        $stored_note->users_id = \Auth::user()->id;
        $stored_note->note_id = $note->id;
        $stored_note->save();
    }

    /**
     * Откат заметки до $id заметки в хранилище изменений
     *
     * @param $id заметки в хранилище
     * @return \Illuminate\Http\RedirectResponse
     */
    public function rollback($id)
    {
        $user_id = Auth::user()->id;
        $stored_note = StoredNote::find($id);
        $note = Note::where('users_id', '=', $user_id)->find($stored_note->note_id);
        $note->name = $stored_note->name;
        $note->text = $stored_note->text;
        $note->updated_at = $stored_note->updated_at;
        $note->created_at = $stored_note->created_at;
        $note->save();
        return \Redirect::to('/note/' . $note->id . '/edit');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $user_id = Auth::user()->id;
        Note::where('users_id', '=', $user_id)->destroy($id);
    }

    /**
     * Удалить заметку
     *
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function remove($id)
    {
        $user_id = Auth::user()->id;
        Note::where('users_id', '=', $user_id)->destroy($id);
        return \Redirect::to('/note/');
    }

    /**
     * Отпаравляет ссылку на заметку
     *
     * @param $id
     * @return string
     */
    public function postLinkItem($id)
    {
        $user_id = Auth::user()->id;

        $hash = "";
        $link = "";
        $note = Note::where('users_id', '=', $user_id)->find($id);
        $json = json_decode(\Request::input('json'), true);
        $emails = isset($json['emails']) ? $json['emails'] : null;
        if(isset($json['need_link']) && $json['need_link'] == true)
        {
            //если ссылки нет в бд
            if(!isset($note->hash_link))
            {
                //сгенерировать ссылку
                $hash = Note::generate_link($id);
                $note->hash_link = $hash;
                $note->save();
            }
            else
            {
                //получить ссылку
                $hash = $note->hash_link;
            }
            $link = 'http://'.$_SERVER['HTTP_HOST'].'/p/'.$hash;
        }
        if(isset($json['emails']) && count($json['emails'])>0)
        {
            foreach($emails as $email)
            {
                \Mail::send('emails.link', ['link' => $link, 'name' => Auth::user()->name], function($message) use($email)
                {
                    $message->to($email, 'test')->subject('Link to note');
                });
            }
        }
        $out_json = [];
        $out_json['link'] = $link;
        return json_encode($out_json);
    }
    /**
     * Сохраняет файл и заметку
     * @param Request $request
     * @return string
     */
    public function postPutFile()
    {
        //создать заметку
        $user_id = Auth::user()->id;
        $note = new \App\Note();
        $note->users_id = $user_id;
        $note->name = \Request::input('name');
        $note->text = \Request::input('text');
        $note->save();

        //получить файлы
        if(\Request::hasFile('files'))
        {
            $files = \Request::file('files');

            foreach($files as $file)
            {
                if($file->isValid())
                {
                    //создать файл в базе данных
                    $notes_file = new NoteFileModel();
                    $notes_file->name = $file->getClientOriginalName();
                    $notes_file->users_id = $user_id;
                    $notes_file->note_id = $note->id;
                    $notes_file->save();
                    //сохранить файл
                    $file->move(storage_path() . '\\users_files\\id' . $user_id . '\\', $file->getClientOriginalName());
                }
                else
                {
                    //TODO добавить и организовать вывод ошибок
                }
            }
        }

        return \Redirect::to('/note/' . $note->id . '/edit');
    }

    /**
     * Отображает хранилище изменений заметки
     *
     * @param $id идентификатор заметки
     * @return \Illuminate\View\View
     */
    public function stat($id)
    {
        $users_id = \Auth::user()->id;
        $stored_notes = StoredNote::
            where('users_id', '=', $users_id)
            ->where('note_id', '=', $id)
            ->get();
        $noteList = Note::getNoteList(Auth::user()->id);

        return view('note.stat',
            [
                'stored_notes' => $stored_notes,
                'note_list' => $noteList

            ]);
    }

    /**
     * Отображает корзину
     *
     * @return \Illuminate\View\View
     */
    public function trash()
    {
        $users_id = \Auth::user()->id;
        $noteList = Note::getNoteList(Auth::user()->id);
        $deleted_notes = Note::select('id', 'name', 'deleted_at')
            ->onlyTrashed()
            ->where('users_id', '=', $users_id)
            ->where('deleted_at', '>=', \DB::raw('DATE_SUB(now(), INTERVAL 1 DAY)'))
            ->orderBy('deleted_at', 'asc')
            ->get();
        return view('note.trash',
        [
            'deleted_notes' => $deleted_notes,
            'note_list' => $noteList
        ]);
    }

    /**
     * Восстанавливает заметку из корзины
     *
     * @param $id идентификатор заметки
     * @return \Illuminate\Http\RedirectResponse
     */
    public function restore($id)
    {
        Note::onlyTrashed()->find($id)->restore();
        return \Redirect::to('/note/' . $id . '/edit');
    }
}
