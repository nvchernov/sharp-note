<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Task;
use App\RepTask;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Contracts\Container;
use Illuminate\Support\Facades\DB;
use App\SNDate;

class TaskController extends Controller {
    const DATE_MYSQL_FORMAT = 'Y-m-d H:i:s';
    const DATE_FULLCALENDAR_FORMAT = 'd.m.Y H:i';

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
        $user_id = Auth::user()->id;
        $tasks = Task::getAllTasksByUserFullcalendar($user_id);

		return view(
            'task.index',
            [
                'tasks' => $tasks
            ]
        );
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view("task.create");
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
    {
        $task = new Task();
        $rep_task = new RepTask();
        $user_id = Auth::user()->id;
        $start_date = 0;
        $end_date = 0;
        $step = new \DateInterval('P1D');

        /* получение запроса */


        $cl_task = \Request::has('json') ? \Request::input('json') : $request->all();

        /* TODO валидация */

        if($cl_task['step_amount'] == "" || $cl_task['step_amount'] <= 1)
        {
            /* создаю ссылку на задачу */
            $task->users_id = $user_id;
            $task->save();

            /* создаю задачу */

            $rep_task->name = $cl_task['name'];
            $rep_task->text = $cl_task['text'];

            $rep_task->start_date =
                date_create_from_format(self::DATE_FULLCALENDAR_FORMAT, $cl_task['start_date'])
                ->format(self::DATE_MYSQL_FORMAT);
            $rep_task->end_date =
                date_create_from_format(self::DATE_FULLCALENDAR_FORMAT, $cl_task['end_date'])
                ->format(self::DATE_MYSQL_FORMAT);

            $rep_task->users_id = $user_id;
            $rep_task->task_id = $task->id;
            $rep_task->save();

        } else {

            $dt_start_date = date_create_from_format(self::DATE_FULLCALENDAR_FORMAT, $cl_task['start_date']);

            /* создание новой задачи */

            $task->users_id = $user_id;
            $task->save();

            /* создается множетсов задач */

            if (isset($cl_task['multiplicity']) && $cl_task['multiplicity'] > 0 &&
                isset($cl_task['step_type']) && $cl_task['step_type'] >= 0 &&
                isset($cl_task['step_amount']) && $cl_task['step_amount'] > 0
            ) {
                if ($cl_task['step_type'] == 0)
                    $step = new \DateInterval('PT' . $cl_task['multiplicity'] * 1 . 'H');
                else if ($cl_task['step_type'] == 1)
                    $step = new \DateInterval('P' . $cl_task['multiplicity'] * 1 . 'D');
                else if ($cl_task['step_type'] == 2)
                    $step = new \DateInterval('P' . $cl_task['multiplicity'] * 7 . 'D');

                $recs = [];

                $tmp_date = $dt_start_date;
                //$tmp_next_date = $tmp_date->add($step);
                for ($i = 0; $i < $cl_task['step_amount']; ++$i) {
                    if($cl_task['step_type'] == 3){
                        $tmp_date = $dt_start_date;
                        if($i>0)
                            $tmp_date = SNDate::addMonths($tmp_date,($i)*$cl_task['multiplicity']);
                    }
                    elseif($cl_task['step_type'] == 4){
                        $tmp_date = SNDate::addMonths($tmp_date,($i)*$cl_task['multiplicity']*12);
                    }
                    else
                    {
                        $tmp_date->add($step);
                    }
                    array_push($recs, [
                            'name' => $cl_task['name'],
                            'text' => $cl_task['text'],
                            'start_date'
                                => $tmp_date->format(self::DATE_MYSQL_FORMAT),
                            'end_date'
                                => $tmp_date->format(self::DATE_MYSQL_FORMAT),
                            'users_id' => $user_id,
                            'task_id' => $task->id
                        ]
                    );

                }
                DB::table('rep_task')->insert($recs);
            }
        }
        /* ответ клиенту */
        $tasks = Task::getAllTasksByUserFullcalendar($user_id);
        return view(
            'task.index',
            [
                'tasks' => $tasks
            ]
        );
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$task = RepTask::find($id);
        $dt_start = date_create_from_format(self::DATE_MYSQL_FORMAT, $task->start_date);
        $task->start_date = $dt_start->format(self::DATE_FULLCALENDAR_FORMAT);
        $dt_end = date_create_from_format(self::DATE_MYSQL_FORMAT, $task->end_date);
        $task->end_date = $dt_start->format(self::DATE_FULLCALENDAR_FORMAT);
        return view("task.edit", ["task" => $task]);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
        $cl_task = \Request::has('json') ? json_decode(\Request::input('json'),true) : \Request::all();
        $user_id = Auth::user()->id;
        $rep_task = RepTask::where('users_id', '=', $user_id)->find($id);
        $ids = [];
        $task_id = $rep_task->task_id;
        $rep_tasks = RepTask::where('task_id', '=', $task_id)
            ->where('users_id', '=', $user_id)
            ->select('id')
            ->get();

        if(\Request::has('json'))
        {
            //Код выполняется когда юзер переместил задачу в редакторе

            $new_date = date_create_from_format(self::DATE_FULLCALENDAR_FORMAT, $cl_task['new_date']);
            $selected_task_date = date_create_from_format(self::DATE_MYSQL_FORMAT, $rep_task->start_date);

            $diff = date_diff($selected_task_date, $new_date);
            if($new_date < $selected_task_date)
                $days_to_move = $diff->d * -1;
            else
                $days_to_move = $diff->d;

            if(isset($cl_task['not_self']) && isset($cl_task['all']))
            {
                foreach ($rep_tasks as $item) {
                    if($item->id != $id)
                        array_push($ids, $item->id);
                }
            }
            elseif(isset($cl_task['self']) && isset($cl_task['next']))
            {
                $arr = RepTask::where('start_date', '>', $rep_task->start_date)
                    ->where('users_id', '=', $user_id)
                    ->where('task_id', '=', $task_id)
                    ->orderBy('start_date', 'desc')
                    ->select('id')
                    ->get()
                    ->toArray();
                array_push($ids, $id);
                foreach($arr as $i)
                    array_push($ids, $i['id']);
            }
            elseif(isset($cl_task['self']) && isset($cl_task['previous']))
            {
                $arr = RepTask::where('start_date', '<', $rep_task->start_date)
                    ->where('users_id', '=', $user_id)
                    ->where('task_id', '=', $task_id)
                    ->orderBy('start_date', 'desc')
                    ->select('id')
                    ->get()
                    ->toArray();
                array_push($ids, $id);
                foreach($arr as $i)
                    array_push($ids, $i['id']);

            }
            elseif(isset($cl_task['all']))
            {
                foreach ($rep_tasks as $item) {
                    array_push($ids, $item->id);
                }
            }
            elseif(isset($cl_task['self']))
            {
                array_push($ids, $id);
            }

            DB::table('rep_task')
                ->whereIn('id', $ids)
                ->update(
                    ['start_date' => DB::raw('DATE_ADD(start_date, INTERVAL ' . $days_to_move . '  DAY)'),
                    'end_date' => DB::raw('DATE_ADD(end_date, INTERVAL ' . $days_to_move . '  DAY)')
                    ]);

            $out_tasks = RepTask::
                select('id', 'start_date', 'end_date')
                ->whereIn('id', $ids)
                ->get()
                ->toArray();

            foreach($out_tasks as $item) {
                $item['start_date'] =
                    date_create_from_format(self::DATE_MYSQL_FORMAT, $item['start_date'])
                        ->format(self::DATE_FULLCALENDAR_FORMAT);

                $item['end_date'] =
                    date_create_from_format(self::DATE_MYSQL_FORMAT, $item['end_date'])
                        ->format(self::DATE_FULLCALENDAR_FORMAT);
            }

            return json_encode($out_tasks, JSON_UNESCAPED_UNICODE);
        }
        else
        {
            $start_date = 0;
            $end_date = 0;
            if (!isset($cl_task['name']) || !isset($cl_task['text']))
                return "{'error':'columns are not defined'}";
            if (isset($cl_task['start_date']) && isset($cl_task['end_date']))
            {
                $dt = date_create_from_format(self::DATE_FULLCALENDAR_FORMAT, $cl_task['start_date']);
                $start_date = $dt->format(self::DATE_MYSQL_FORMAT);
                $dt = date_create_from_format(self::DATE_FULLCALENDAR_FORMAT,$cl_task['end_date']);
                $end_date = $dt->format(self::DATE_MYSQL_FORMAT);
            }
            $rep_task->name = $cl_task['name'];
            $rep_task->text = $cl_task['text'];
            $rep_task->start_date = $start_date;
            $rep_task->end_date = $end_date;
            $rep_task->users_id = $user_id;
            $rep_task->save();
        }
        $user_id = Auth::user()->id;
        $tasks = Task::getAllTasksByUserFullcalendar($user_id);

        return view(
            'task.index',
            [
                'tasks' => $tasks
            ]
        );
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
        $user_id = Auth::user()->id;
		if(\Request::has('json'))
        {
            $cl_params = json_decode(\Request::input('json'),true);
            $rep_task = RepTask::where('users_id', '=', $user_id)->find($id);
            $task_id = $rep_task->task_id;
            $ids = [];

            $rep_tasks = RepTask::where('users_id', '=', $user_id)
                ->where('task_id', '=', $task_id)
                ->select('id')
                ->get();

            if(isset($cl_params['not_self']) && isset($cl_params['all']))
            {
                foreach ($rep_tasks as $item) {
                    if($item->id != $id)
                        array_push($ids, $item->id);
                }
            }
            elseif(isset($cl_params['self']) && isset($cl_params['next']))
            {
                $arr = RepTask::where('start_date', '>', $rep_task->start_date)
                    ->where('users_id', '=', $user_id)
                    ->where('task_id', '=', $task_id)
                    ->orderBy('start_date', 'desc')
                    ->select('id')
                    ->get()
                    ->toArray();
                array_push($ids, $id);
                foreach($arr as $i)
                    array_push($ids, $i['id']);
            }
            elseif(isset($cl_params['self']) && isset($cl_params['previous']))
            {
                $arr = RepTask::where('start_date', '<', $rep_task->start_date)
                    ->where('users_id', '=', $user_id)
                    ->where('task_id', '=', $task_id)
                    ->orderBy('start_date', 'desc')
                    ->select('id')
                    ->get()
                    ->toArray();
                array_push($ids, $id);
                foreach($arr as $i)
                    array_push($ids, $i['id']);
            }
            elseif(isset($cl_params['all']))
            {
                foreach ($rep_tasks as $item) {
                    array_push($ids, $item->id);
                }
            }
            elseif(isset($cl_params['self']))
            {
                array_push($ids, $id);
            }

            RepTask::whereIn('id', $ids)
                ->where('users_id', '=', $user_id)
                ->delete();

            $out_json = ['deleted' => $ids];

            return json_encode($out_json, JSON_UNESCAPED_UNICODE);

        }
	}

    public function postLinkItem($id)
    {
        $user_id = Auth::user()->id;
        $out_json = [];
        $hash = "";
        $link = "";
        $rep_task = RepTask::where('users_id', '=', $user_id)->find($id);
        $json = json_decode(\Request::input('json'), true);
        $emails = isset($json['emails']) ? $json['emails'] : null;
        if(isset($json['need_link']) && $json['need_link'] == true)
        {
            //$note = Note::find($id);
            //если ссылки нет в бд
            if(!isset($rep_task->hash_link))
            {
                //сгенерировать ссылку
                $hash = \App\Note::generate_link($id);
                $rep_task->hash_link = $hash;
                $rep_task->save();
            }
            else
            {
                //получить ссылку
                $hash = $rep_task->hash_link;
            }
            $link = 'http://'.$_SERVER['HTTP_HOST'].'/t/'.$hash;
        }
        if(isset($json['emails']) && count($json['emails'])>0)
        {
            foreach($emails as $email)
            {
                \Mail::send('emails.link', ['link' => $link, 'name' => Auth::user()->name], function($message) use($email)
                {
                    $message->to($email, 'test')->subject('Link to note');
                });
            }
        }
        $out_json['link'] = $link;
        return response()->json($out_json);
    }

}
