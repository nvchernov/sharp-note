<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class DownloadController extends Controller {
    /**
     * Позволяет пользователю скачивать файлы
     * @param Request $request
     * @return mixed
     */
    public function getFile(Request $request)
    {
        $rep = new \App\Repository();

        $file_name =  $rep->get_file($request->user()->id, $request->segment(2))->name;

        $file = storage_path() . '\\users_files\\id' . $request->user()->id . '\\' . $file_name;
        return response()->download($file);
    }

}
