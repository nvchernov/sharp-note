<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::controllers([
    'auth' => 'Auth\AuthController',
    'password' => 'Auth\PasswordController',
]);
//Route::post('/paper', function (){});
//Route::get('/test','HomeController@test');

Route::get('/p/{hash}','PublicController@getNote');
Route::get('/t/{hash}','PublicController@getTask');

//Route::controller('/note/{method?}/{id?}/','NoteController');
//Route::controller('/note/{id?}/','NoteController');
    //->where(['id' => '[0-9]+']);
Route::get('/note/trash','NoteController@trash');
Route::get('/note/{id}/remove','NoteController@remove');
Route::get('/note/{id}/restore','NoteController@restore');
Route::resource('note','NoteController');

Route::get('/note/{id}/stat','NoteController@stat');
Route::get('/note/{id}/rollback','NoteController@rollback');
Route::post('/note/link_item/{id}','NoteController@postLinkItem');
Route::post('/task/link_item/{id}','TaskController@postLinkItem');

Route::get('/statistic/note','StatisticController@noteStatistic');
Route::get('/statistic/task','StatisticController@taskStatistic');

Route::resource('task','TaskController');
Route::resource('file','FileController');
Route::controller('file','FileController');

Route::post('/test{name?}','HomeController@posttest');
Route::get('/download/{id}','DownloadController@getFile')
    ->where(['id' => '[0-9]+']);

Route::post('/note/put_file','NoteController@postPutFile');
Route::post('/paper/link_item','PaperController@postLinkItem');
Route::post('/paper/get_files_list','PaperController@postGetFiles');
Route::post('/paper/new_item','PaperController@postNewItem');
Route::post('/paper/remove_item','PaperController@postRemoveItem');
Route::post('/paper/save_item','PaperController@postSaveItem');
Route::controller('/paper{id?}', 'PaperController');
Route::get('/paper/new', 'PaperController@getNew');
Route::get('home', 'HomeController@index');
Route::get('/', 'WelcomeController@index');
Route::controller('/', 'HomeController');

//Route::get('paper', 'PaperController@index');



