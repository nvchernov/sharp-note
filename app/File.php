<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class File extends Model {

    protected $table = 'file';

    /**
     * Получить список файлов по id пользователя и id заметки
     *
     * @param $user_id
     * @param $note_id
     * @return mixed
     */
    public static function getFileList($user_id, $note_id)
    {
        return static::select('id','name')
            ->where('users_id', '=',  $user_id)
            ->where('note_id', '=',  $note_id)
            ->get();
    }

    /**
     * Получить файл по id пользователя и id заметки
     *
     * @param $user_id
     * @param $file_id
     * @return mixed
     */
    public function getFile($user_id, $file_id)
    {
        return NoteFileModel::where('users_id', '=', $user_id)
            ->where('id', '=', $file_id)
            ->select('id', 'name')
            ->first();
    }
}
