<?php namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Класс задачи
 *
 * Class RepTask
 * @package App
 */
class RepTask extends Model {

	protected $table = 'rep_task';

    /**
     * Получить задачу по ее hash'у
     * @param $hash
     * @return mixed
     */
    public static function get_task_by_link($hash)
    {
        return static::where('hash_link', '=', $hash)
            ->first();
    }

    public static function moveTasks($rep_tasks, $user_id, $flag, $new_date)
    {

    }
}
