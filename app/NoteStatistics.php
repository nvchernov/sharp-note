<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class NoteStatistics extends Model {
    protected $table = 'note_statistics';
    public $timestamps = false;

    /**
     * Получить статистику просмотров заметки по id пользователя
     *
     * @param $user_id
     * @return array|static[]
     */
	public static function getStatisticByUser($user_id)
    {
        return \DB::table('note_statistics')
            ->join('paper', 'note_statistics.note_id', '=', 'paper.id')
            ->where('paper.users_id', '=', $user_id)
            ->join('users', 'note_statistics.users_id', '=', 'users.id')
            ->select('paper.name as note_name', 'note_statistics.note_id as note_id', 'users.name as user_name', 'note_statistics.datetime as datetime')
            ->orderBy('note_id')
            ->get();
    }

    /**
     * Добавить просмотр заметки
     *
     * @param $user|null
     * @param $note
     */
    public static function addStatistic($user, $note)
    {
        $statistic = new static();
        $statistic->users_id = isset($user) ? $user->id : null;
        $statistic->note_id = $note->id;
        $statistic->datetime = date("Y-m-d H:i:s");
        $statistic->save();
    }
}
