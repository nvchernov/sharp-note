<?php
/**
 * User: Nikolay
 * Date: 08.06.2015
 * Time: 23:39
 */

namespace app;
class SNDate {
    //функция добовляет месяц к текущей дате, при этом учитывается високосный год
    static function addMonths($date,$months){
        $monthToAdd = $months;
        $year = $date->format('Y');
        $month = $date->format('m');
        $day = $date->format('d');

        $year += floor($monthToAdd/12);
        $monthToAdd = $monthToAdd%12;
        $month += $monthToAdd;
        if($month > 12) {
            $year ++;
            $month = $month % 12;
            if($month === 0)
                $month = 12;
        }

        if(!checkdate($month, $day, $year)) {
            $d2 = \DateTime::createFromFormat('Y-n-j', $year.'-'.$month.'-1');
            $d2->modify('last day of');
        }else {
            $d2 = \DateTime::createFromFormat('Y-n-d', $year.'-'.$month.'-'.$day);
        }
        $d2->setTime($date->format('H'), $date->format('i'), $date->format('s'));
        return $d2;
    }
} 