<?php namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Класс
 *
 * Class Task
 * @package App
 */
class Task extends Model {

	protected $table = 'task';

    /**
     * Получить все задачи пользователя в формате, который принимает fullcalendar
     *
     * @param $user_id
     * @return array|null
     */
    public static function getAllTasksByUserFullcalendar($user_id)
    {
        return
            \DB::table('task')
                ->join(
                    'rep_task',
                    'task.id',
                    '=',
                    'rep_task.task_id')
                ->select(
                    'rep_task.id as id',
                    'task.id as tid',
                    'rep_task.name as title',
                    'rep_task.start_date as start',
                    'rep_task.end_date as end')
                ->where('rep_task.users_id', '=', $user_id)
                ->distinct()
                ->get();
    }
}
