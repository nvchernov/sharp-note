<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class TaskStatistics extends Model {
    protected $table = 'task_statistics';
    public $timestamps = false;

    /**
     * Получить статистику просмотров
     *
     * @param $user_id
     * @return array|static[]
     */
    public static function getStatisticByUser($user_id)
    {
        return \DB::table('task_statistics')
            ->join('rep_task', 'task_statistics.task_id', '=', 'rep_task.id')
            ->where('rep_task.users_id', '=', $user_id)
            ->join('users', 'task_statistics.users_id', '=', 'users.id')
            ->select('rep_task.name as task_name', 'task_statistics.task_id as task_id', 'users.name as user_name', 'task_statistics.datetime as datetime')
            ->orderBy('task_id')
            ->get();
    }

    /**
     * Добавляет новую запись в бд
     *
     * @param $user
     * @param $task
     */
    public static function addStatistic($user, $task)
    {
        $statistic = new static();
        $statistic->users_id = isset($user) ? $user->id : null;
        $statistic->task_id = $task->id;
        $statistic->datetime = date("Y-m-d H:i:s");
        $statistic->save();
    }
}
