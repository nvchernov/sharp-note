<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Note extends Model {
    protected $table = 'paper';

    use \Illuminate\Database\Eloquent\SoftDeletes;

    protected $dates = ['deleted_at'];

    /**
     * Данные из бд для списка заметок
     * @param $user_id
     * @return mixed
     */
    public static function getNoteList($user_id)
    {
        return static::select('id','name')
            ->where('users_id', '=', $user_id)
            ->get();
    }

    /**
     * Удаляет несколько заметок
     *
     * @param $user_id
     * @param $note_ids Array
     */
    public static function destroyMany($user_id, $note_ids)
    {
        return static::where('users_id', '=', $user_id)
            ->wherein('note_id', '=', $note_ids)
            ->delete();
    }

    /**
     * Сгенерировать ссылку на заметку
     * @param $note_id
     * @return string
     */
    public static function generate_link($note_id)
    {
        $link = hash("crc32","}^".$note_id."~4");
        printf("%u\n", $link);
        return $link;
    }

    /**
     * Получить заметку по ее hash'у
     * @param $hash
     * @return mixed
     */
    public static function get_paper_by_link($hash)
    {
        return static::where('hash_link', '=', $hash)
            ->first();
    }

    /**
     * Закрепляет к заметке новый файл
     *
     * @param $file
     */
    public function addFile($file)
    {
        //создать файл в базе данных
        $notes_file = new NoteFileModel();
        $notes_file->name = $file->getClientOriginalName();
        $notes_file->users_id = Auth::user()->id;
        $notes_file->paper_id = $this->id;
        $notes_file->save();
        //сохранить файл
        $file->move(
            storage_path() . '\\users_files\\id' . $notes_file->users_id . '\\',
            $file->getClientOriginalName());
    }
}
