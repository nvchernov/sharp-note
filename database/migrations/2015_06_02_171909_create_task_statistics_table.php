<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTaskStatisticsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('task_statistics', function(Blueprint $table)
		{
			$table->increments('id');
            $table->timestamp('datetime');

            //внешние ключи
            $table->integer('task_id')->unsigned();
            $table->foreign('task_id')->references('id')->on('rep_task')->onDelete('cascade');;

            $table->integer('users_id')->unsigned();
            $table->foreign('users_id')->references('id')->on('users');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('task_statistics');
	}

}
