<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNoteStatisticsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('note_statistics', function(Blueprint $table) {
            $table->increments('id');
            $table->timestamp('datetime');

            //внешние ключи
            $table->integer('note_id')->unsigned();
            $table->foreign('note_id')->references('id')->on('paper')->onDelete('cascade');

            $table->integer('users_id')->unsigned()->nullable();
            $table->foreign('users_id')->references('id')->on('users');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('note_statistics');
	}

}
