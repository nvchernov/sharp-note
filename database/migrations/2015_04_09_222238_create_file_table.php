<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFileTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('file', function(Blueprint $table)
		{
			$table->increments('id');
            $table->timestamp('created_at');
            $table->timestamp('updated_at');

            $table->string('name', 100);

            //Внешние ключи
            $table->integer('users_id')->unsigned();
            $table->foreign('users_id')->references('id')->on('users');

            //Внешние ключи
            $table->integer('note_id')->unsigned();
            $table->foreign('note_id')->references('id')->on('paper');


		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('file');
	}

}
