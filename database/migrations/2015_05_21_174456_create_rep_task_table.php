<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRepTaskTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('rep_task', function(Blueprint $table)
		{
            $table->increments('id');
            $table->timestamp('created_at');
            $table->timestamp('updated_at');
            $table->string('name', 100);
            $table->text('text');   //текст заметки
            $table->string('hash_link', 33)->nullable();

            $table->timestamp('start_date');
            $table->timestamp('end_date');

            //кратность
            $table->integer('multiplicity')->nullable();
            //тип шага
            //0 - час, 1 - день, 2 - неделя, 3 - месяц, 4 - год
            $table->integer('step_type')->nullable();
            //количество шагов
            $table->integer('step_amount')->nullable();

            //Внешние ключи
            $table->integer('users_id')->unsigned();
            $table->foreign('users_id')->references('id')->on('users');

            $table->integer('task_id')->unsigned();
            $table->foreign('task_id')->references('id')->on('task');

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('rep_task');


	}

}
