<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStoredNotesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('stored_notes', function(Blueprint $table)
		{
			$table->increments('id');

            $table->string('name', 100);
            $table->text('text');   //текст задачи

            //Внешние ключи
            $table->integer('users_id')->unsigned();
            $table->foreign('users_id')->references('id')->on('users');

            //Внешние ключи
            $table->integer('note_id')->unsigned();
            $table->foreign('note_id')->references('id')->on('paper');

			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('stored_notes');
	}

}
