var NoteForm = function(noteId) {
    if('undefined' == noteId)
        return;

    this.noteId = noteId;
    this.save = function() {
        json = {
            'text' : this.CText.val(),
            'name' : this.CName.val()
        };
        $.ajax({
            type: 'PUT',
            url: '/note/' + this.noteId,
            data: '_token=' + csrft._token + '&json=' + JSON.stringify(json),
            success : function(){}
        });
    };
    this.removeNote = function(id){
        $.ajax({
            type: 'DELETE',
            url: '/note/' + id,
            data: '_token=' + csrft._token,
            success : function(){
                NoteForm.removeNoteFromList(id);
            }
        });
    };
    NoteForm.removeNoteFromList = function(id){
        $('[note-id="'+id+'"]').remove();
    };
    this.linkTextBox = new Control({id:"CLink"});
    this.mailList = new Control({id:"CMailList"});

    this.getLinkAjax = function(){
        //return {id:'2', emails:['q-cs@ya.ru','vital@ya.ru'], link:'http://sharp-note.loc/p/35dsh5a43'};
        var json = {
            'emails': this.mailList.d.tagsinput('items').length > 0 ? this.mailList.d.tagsinput('items') : null,
            'need_link': this.linkTextBox.d.val() == "" ? true : true
        };
        $.ajax({
            type: 'POST',
            url: '/note/link_item/' + this.noteId,
            data: '_token=' + csrft._token + '&json=' + JSON.stringify(json),
            success: function(msg1){
                var resp = JSON.parse(msg1);
                //TODO изменить на this.linkTextBox.d.val(resp.link);
                $("#CLink").val(resp.link);
            }
        });
    };

    this.editForm = new Control({id:"CEditForm"});
    this.fileForm = new Control({id:"CFileForm"});
    this.linkForm = new Control({id:"CLinkForm"});

    this.toggleNoteForm = function(name){
        switch (name)
        {
            case 'edit' :
                this.editForm.d.show();
                this.fileForm.d.hide();
                this.linkForm.d.hide();
                break;
            case 'file' :
                this.editForm.d.hide();
                this.fileForm.d.show();
                this.linkForm.d.hide();
                break;
            case 'link' :
                this.editForm.d.hide();
                this.fileForm.d.hide();
                this.linkForm.d.show();
                break;
            default : break;
        }
    };

    this.linkBtn = new Control({id:"CGetLinkBtn", click:$.proxy(function(){this.getLinkAjax()},this)});

    //переключатель формы
    this.editFormButton = new Control({id:"CEditButton", click:$.proxy(function(){this.toggleNoteForm('edit');},this)});
    this.fileFormButton = new Control({id:"CFileButton", click:$.proxy(function(){this.toggleNoteForm('file');},this)});
    this.linkFormButton = new Control({id:"CLinkButton", click:$.proxy(function(){this.toggleNoteForm('link');},this)});
    //кнопка сохранить
    this.CSaveButton = new Control({id:"CNoteSave", click: $.proxy(this.save, this)});
    //поле ввода для имени заметки
    this.CName = new Control({id:"CNoteName"});
    //поле ввода для текста заметки
    this.CText = new Control({id:"CNoteText"});



};